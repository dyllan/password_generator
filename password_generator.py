import string
from random import choice


def password_generator(password_length: int) -> str:
    """ Takes an integer as an argument and returns a random string of letters, 
    digits and punctuation. """
    try:
        if password_length > 100:
            print("Length of password must not exceed 100 characters.")
        else:
            return "".join([choice(string.ascii_letters + string.digits +
                                   string.punctuation) for _ in range(password_length)])
    except Exception:
        print("Provide a number as an argument.")


if __name__ == "__main__":
    password_generator(password_length='')
