# password_generator

Python random password generator using a combination of letters, digits and punctuation.

# usage
# import
from password_generator import password_generator


# call function with integer
password_generator(12)


# limitations
max password length should not exceed 100 characters.
